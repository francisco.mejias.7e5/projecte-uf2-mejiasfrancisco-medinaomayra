package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model
import kotlinx.serialization.Serializable

@Serializable
data class Film(
    val filmName: String,
    val director: String,
    val mainActor: String,
    val genre: String,
    val length: Int
)

