package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItbStorage
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.User

data class AppState(var currentUser: User?) {
    val filmItb = FilmItbStorage().load() ?: FilmItb()
}