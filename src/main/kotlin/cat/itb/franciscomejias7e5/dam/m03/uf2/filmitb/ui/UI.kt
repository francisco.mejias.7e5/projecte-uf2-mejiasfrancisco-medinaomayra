package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItbStorage
import java.util.*

data class UI(val scanner: Scanner = Scanner(System.`in`)) {
    val appState = AppState(null)
    val filmItb = appState.filmItb
    val userUI = UserUI(scanner, filmItb, appState)
    val filmUI = FilmUI(scanner, filmItb, appState)
    val searchUI = SearchUI(scanner, filmItb, appState)
    private val welcome = "Welcome to FilmItb:\n" +
                          "1: User\n" +
                          "2: Films\n" +
                          "3: Search\n" +
                          "0: Exit"

    fun start() {
        var action = -1

        while (action != 0){
            userUI.firstAddUserOrCheckIfExist()
            while (action != 0){
                action = welcomeMenu()
                action = when(action){
                    1 -> {
                        userUI.showMenu(action)
                    }
                    2 -> {
                        filmUI.showMenu(action)
                    }
                    3 -> {
                        searchUI.showMenu(action)
                    }
                    else -> {
                        FilmItbStorage().save(filmItb)
                        0
                    }
                }
            }

        }
    }

    private fun welcomeMenu(): Int {
        println(welcome)
        return scanner.nextLine().toInt()
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    UI(scanner).start()
}