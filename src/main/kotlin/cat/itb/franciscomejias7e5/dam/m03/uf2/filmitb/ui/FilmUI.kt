package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui

import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.Film
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.ResultFilmItbFilms
import java.util.*

data class FilmUI(val scanner: Scanner, val filmItb: FilmItb, val appState: AppState) {

    private fun selectFilm(message : String?): Film? {
        printMessage(message)
        val filmName = scanner.nextLine()
        if (filmName.isBlank()){
            println("You can't leave the field empty")
            return selectFilm(message)
        }
        if (filmName.lowercase() == "exit") {
            return null
        }
        val films = filmItb.findFilmByName(filmName)
        return if (films.size == 1) {
            films.first()
        } else if (films.size > 1) {
            return selectFilmFromSubList(films)
        } else {
            println("This film not exist, try again")
            return selectFilm(message)
        }
    }

    private fun printMessage(message: String?) {
        message?.let{
            println(it)
        }
    }

    private fun selectFilmFromSubList(films: MutableList<Film>): Film {
        println("There is many option choose one of them")
        printFilmsWithIndex(films)
        val choose = scanner.nextLine().toInt() - 1
        return films[choose]
    }

    /** A revisar
     * La función en sí no funciona, en el momento en el que se vuelve a llamar a la misma función
     * el valor deja de tener concordancia con el que escribe el usuario
     *
     * private fun insistOnPickInt(message: String? = null): Int {
     *     try {
     *         printMessage(message)
     *         return scanner.nextLine().toInt()
     *
     *         /** A revisar
     *          * Sucede un error cuando se comprueba si el número está entre los valores posibles.
     *          * No sé por qué cuando se cumple la condición y el valor sí que está entre los dos
     *          * valores posibles no ejecuta el return y, en cambio, ejecuta la última acción del
     *          * código que está en el else, lo raro es que cuando "ejecuta" esa acción vuelve al
     *          * return pero con un valor semi-aleatorio. ¯\_(ツ)_/¯
     *          *
     *          * if (choose in minResult..maxResult) {
     *          * return choose
     *          * } else {
     *          * println("The number must there between $minResult - $maxResult")
     *          * insistOnPickIntSelectFilm(message, minResult, maxResult)
     *          * }
     *          */
     *     } catch (e: Exception) {
     *         println("You must write a number")
     *         insistOnPickInt(message)
     *     }
     *     return 0
     * }
    */

    private fun printFilmsWithIndex(films: MutableList<Film>) {
        films.forEachIndexed { i, it ->
            println("${i+1} -> $it")
        }
    }

    fun addFilm() {
        var end = false
        while (!end){
            println("Enter a film name or type exit to quit")
            val filmName = scanner.nextLine()
            if (filmName.isBlank()){
                println("You can't leave the field empty")
            } else {
                if (filmName.lowercase() == "exit") {
                    end = true
                } else {
                    println("Enter a director")
                    val director = scanner.nextLine()
                    println("Enter a main actor")
                    val mainActor = scanner.nextLine()
                    println("Enter a genere")
                    val genre = scanner.nextLine()
                    println("Enter a length")
                    val length = scanner.nextLine().toInt()
                    when (filmItb.addFilm(filmName, director, mainActor, genre, length)) {
                        ResultFilmItbFilms.CORRECT -> end = true
                        ResultFilmItbFilms.ERROR_EXISTING_FILM ->
                            println("You can't add a film that exist, try again or type exit to quit")
                    }
                }
            }
        }
    }

    fun showFilms() {
        println(filmItb.showFilms() ?: "There is no film yet")
    }

    fun deleteFilm(){
        selectFilm("Enter film name you want to delete or type exit to quit")?.let {
            filmItb.removeFilm(it)
        }
    }

    fun watchFilm() {
        if (filmItb.showFilms() != null) {
            selectFilm("Enter film name you want to watch or type exit to quit")?.let {
                println(it)
                filmItb.addToWatchedFilms(it, appState)
            }
        } else {
            println("There is no film yet")
        }
    }

    fun viewWatchedFilms() {
        println(filmItb.showWatchedFilms(appState) ?: "You haven't seen any film yet")
    }

    fun addFilmToFavorites() {
        selectFilm("Enter film name you want to add to your favourites or type exit to quit")?.let {
            filmItb.addFilmToFavorites(it, appState)
        }
    }

    fun showFavorites() {
        println(filmItb.showFavoritesFilm(appState) ?: "No favorite movie yet")
    }

    fun showLikesPerFilm() {
        var end = false
        while (!end) {
            println("Do you want to see the likes of all the movies or just one?")
            println("1 -> One\n2 -> All\nExit -> To quit")
            val choose = scanner.nextLine()
            if (choose.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (choose.lowercase() == "exit") {
                    end = true
                } else {
                    if (choose.toInt() == 1) {
                        selectFilm("Enter the name of the film you want to know how many likes it has, or type exit to leave")?.let {
                            println(filmItb.likesOfThisFilm(it))
                            end = true
                        }
                    } else {
                        println(filmItb.likesOfAllFilms())
                        end = true
                    }
                }
            }
        }
    }

    private val films = "Films:\n" +
            "1: Add film\n" +
            "2: Show films\n" +
            "3: Delete films\n" +
            "4: Watch films\n" +
            "5: View watched films\n" +
            "6: Add film to favorites\n" +
            "7: Show favorites\n" +
            "8: Show likes per film\n" +
            "0: Return to main menu"

    private fun filmMenu(): Int {
        println(films)
        return scanner.nextLine().toInt()
    }

    fun showMenu(actionExt: Int): Int {
        var action = actionExt
        while (action != 0){
            action = filmMenu()
            when (action){
                1 -> addFilm()
                2 -> showFilms()
                3 -> deleteFilm()
                4 -> watchFilm()
                5 -> viewWatchedFilms()
                6 -> addFilmToFavorites()
                7 -> showFavorites()
                8 -> showLikesPerFilm()
                0 -> {
                    action = -1
                    break
                }
            }
            action = -1
        }
        return action
    }
}