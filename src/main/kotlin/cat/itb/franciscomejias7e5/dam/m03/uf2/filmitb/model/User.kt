package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model
import kotlinx.serialization.Serializable

@Serializable
data class User (var name: String)