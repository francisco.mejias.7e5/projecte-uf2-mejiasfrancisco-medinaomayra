package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.io.path.*


class FilmItbStorage{
    private val homePath: String = System.getProperty("user.home")
    private val filePath = Path("$homePath/.FilmItbData")

    fun save(filmItb: FilmItb){
        try {
            filePath.createFile()
        } catch (_: Exception) {
        }
        val json = Json.encodeToString(filmItb)
        filePath.writeText(json)
    }
    fun load() =
        if (filePath.exists()) {
            val filmItb: FilmItb = Json.decodeFromString(filePath.readText())
            filmItb
        } else {
            null
        }
}