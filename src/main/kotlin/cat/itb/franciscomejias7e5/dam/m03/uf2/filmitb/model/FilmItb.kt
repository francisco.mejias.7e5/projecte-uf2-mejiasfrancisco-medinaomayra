package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui.AppState
import kotlinx.serialization.Serializable

enum class ResultFilmItbUsers{CORRECT, ERROR_NOT_EXISTING, ERROR_CURRENT_USER, ERROR_EXISTING_USER}
enum class ResultFilmItbFilms{CORRECT, ERROR_EXISTING_FILM}

@Serializable
//TODO("Error Mapas No Existentes en JSON")
data class FilmItb(val users: MutableList<User> = mutableListOf(),
                   val films: MutableList<Film> = mutableListOf(),
                   val watchedFilms: MutableMap<User, MutableMap<Film, Int>> = mutableMapOf(),
                   val favoritesFilms: MutableMap<User, MutableMap<Film, Int>> = mutableMapOf()) {
    fun addFilm(
        filmName: String,
        director: String,
        mainActor: String,
        genre: String,
        length: Int
    ): ResultFilmItbFilms {
        val film = Film(filmName, director, mainActor, genre, length)
        findIfExistSameFilm(film)?.let {
            return ResultFilmItbFilms.ERROR_EXISTING_FILM
        }
        films += film
        return ResultFilmItbFilms.CORRECT
    }

    fun showFilms(): String? {
        return if (films.isEmpty()) {
            null
        } else {
            userFriendlyFilmsResult()
        }
    }

    /**
     * Make the film list more easily to see for common users
     * @return Films separated by enters
     */
    fun userFriendlyFilmsResult(): String {
        var filmsToReturn = ""
        for (i in 0 until films.lastIndex) {
            filmsToReturn += "${films[i]}\n"
        }
        filmsToReturn += films.last()
        return filmsToReturn
    }

    fun removeFilm(film: Film) {
        films.remove(film)
    }

    fun addToWatchedFilms(film: Film, appState: AppState) {
        val currentUser = appState.currentUser!!
        val userIt = findUserInMap(watchedFilms, currentUser)

        if (userIt == null) {
            watchedFilms += mutableMapOf(currentUser to mutableMapOf())
            addOrPlusFilmInUser(watchedFilms[currentUser]!!, film)
        } else {
            addOrPlusFilmInUser(userIt, film)
        }
    }

    /**
     * Add a film in user list of films or if exist increase his value: number of times seen
     */
    private fun addOrPlusFilmInUser(user: MutableMap<Film, Int>, film: Film) {
        for (filmIt in user) {
            if (filmIt.key == film) {
                filmIt.value.plus(1)
                break
            }
        }
        user += film to 1
    }

    private fun findUserInMap(map: MutableMap<User, MutableMap<Film, Int>>, user: User): MutableMap<Film, Int>? {
        for (userIt in map) {
            if (userIt.key == user) {
                return userIt.value
            }
        }
        return null
    }

    fun showWatchedFilms(appState: AppState): String? {
        return if (watchedFilms.isEmpty()) {
            null
        } else {
            userFriendlyMapUFResult(watchedFilms, appState.currentUser!!, "This user hasn't seen any film yet")
        }
    }

    fun addFilmToFavorites(film: Film, appState: AppState) {
        val currentUser = appState.currentUser!!
        val userIt = findUserInMap(favoritesFilms, currentUser)

        if (userIt == null) {
            favoritesFilms += mutableMapOf(currentUser to mutableMapOf())
            addToFavorite(favoritesFilms[currentUser]!!, film)
        } else {
            addToFavorite(userIt, film)
        }
    }

    private fun addToFavorite(user: MutableMap<Film, Int>, film: Film) {
        user += film to 1
    }


    fun showFavoritesFilm(appState: AppState): String? {
        return if (favoritesFilms.isEmpty()) {
            null
        } else {
            userFriendlyMapUFResult(favoritesFilms, appState.currentUser!!, "This user didn't have any favourite film yet")
        }
    }

    private fun userFriendlyMapUFResult(map: MutableMap<User, MutableMap<Film, Int>>, currentUser: User, message: String): String {
        var filmsToReturn = ""
        val userIt = findUserInMap(map, currentUser)
        userIt?.let {
            for (filmIt in userIt) {
                filmsToReturn += "${filmIt.key}\n"
            }
            return filmsToReturn.dropLast(1)
        }
        return message
    }

    fun likesOfThisFilm(film: Film): String {
        var likes = 0
        for (userIt in favoritesFilms.values) {
            for (filmIt in userIt) {
                if (filmIt.key == film) {
                    likes ++
                }
            }
        }
        return "$film have $likes likes"
    }

    fun likesOfAllFilms(): String {
        val likes = mutableMapOf<Film, Int>()
        var stringToOutput = ""
        for (userIt in favoritesFilms.values) {
            for (filmIt in userIt) {
                if (filmIt.key in likes.keys) {
                    for (like in likes) {
                        if (like.key == filmIt.key) {
                            likes += like.key to like.value+1
                        }
                    }
                } else {
                    likes += filmIt.key to filmIt.value
                }
            }
        }
        likes.forEach{stringToOutput += "${it.key} have ${it.value} likes\n"}
        return stringToOutput.dropLast(1)
    }

    fun findIfExistSameFilm(film: Film): Film? {
        return films.find { it == film }
    }

    /** USERS **/

    fun addUser(name: String): ResultFilmItbUsers {
        findUserByName(name)?.let {
            return ResultFilmItbUsers.ERROR_EXISTING_USER
        }
        val user = User(name)
        users += user
        return ResultFilmItbUsers.CORRECT
    }

    val showUser get() = users

    fun updateUser(userNameToChange: String, newUserName: String) {
        val user = findUserByName(userNameToChange)
        user?.name = newUserName
    }

    fun removeUser(name: String, appState: AppState): ResultFilmItbUsers {
        findUserByName(name)?.let {
            if (it != appState.currentUser){
                users.remove(it)
                return ResultFilmItbUsers.CORRECT
            }
            return ResultFilmItbUsers.ERROR_CURRENT_USER
        }
        return ResultFilmItbUsers.ERROR_NOT_EXISTING
    }

    fun changeUser(name: String, appState: AppState): ResultFilmItbUsers {
        val user = findUserByName(name)
        findUserByName(name)?.let {
            if (user != appState.currentUser) {
                appState.currentUser = it
                return ResultFilmItbUsers.CORRECT
            }
            return ResultFilmItbUsers.ERROR_CURRENT_USER
        }
        return ResultFilmItbUsers.ERROR_NOT_EXISTING
    }

    fun findUserByName(name: String): User? {
        return users.find { it.name == name }
    }

    /** SEARCH **/

    fun findFilmByName(filmName: String): MutableList<Film> {
        val filmsWithThisName = mutableListOf<Film>()
        for (film in films){
            if (film.filmName == filmName) {
                filmsWithThisName += film
            }
        }
        return filmsWithThisName
    }

    fun findFilmByDirector(director: String): MutableList<Film> {
        val filmsWithThisDirector = mutableListOf<Film>()
        for (film in films){
            if (film.director == director) {
                filmsWithThisDirector += film
            }
        }
        return filmsWithThisDirector
    }

    fun findFilmByMainActor(mainActor: String): MutableList<Film> {
        val filmsWithThisMainActor = mutableListOf<Film>()
        for (film in films){
            if (film.mainActor == mainActor) {
                filmsWithThisMainActor += film
            }
        }
        return filmsWithThisMainActor
    }

    fun findFilmByGenre(genre: String): MutableList<Film> {
        val filmsWithThisGenre = mutableListOf<Film>()
        for (film in films){
            if (film.genre == genre) {
                filmsWithThisGenre += film
            }
        }
        return filmsWithThisGenre
    }

    fun findFilmByLength(length: Int): MutableList<Film> {
        val filmsWithThisLength = mutableListOf<Film>()
        for (film in films){
            if (film.length == length) {
                filmsWithThisLength += film
            }
        }
        return filmsWithThisLength
    }

    fun filmsNotWatchedByThisUser(appState: AppState): String? {
        if (films.isEmpty()) {
            return null
        }
        val filmsNotWatched = mutableListOf<Film>()
        val currentUser = appState.currentUser
        for (film in films) {
            if (watchedFilms[currentUser]!!.keys.find { it == film } == null) {
                filmsNotWatched += film
            }
        }
        return if (filmsNotWatched.isEmpty()) {
            "You see all the movies"
        } else {
            var toOutput = ""
            filmsNotWatched.forEach{toOutput += "$it\n"}
            toOutput.dropLast(1)
        }
    }
}


