package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItb
import java.util.*

data class SearchUI(val scanner: Scanner, val filmItb: FilmItb, val appState: AppState) {

    fun searchFilmsByTitle() {
        var end = false
        while (!end) {
            println("Enter the name of the film to see films with that name, or type exit to quit")
            val filmName = scanner.nextLine()
            if (filmName.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (filmName.lowercase() == "exit") {
                    end = true
                } else {
                    val films = filmItb.findFilmByName(filmName)
                    if (films.isEmpty()) {
                        println("No yet films with that name")
                        end = true
                    } else {
                        var toOutput = ""
                        films.forEach{toOutput += "$it\n"}
                        println(toOutput.dropLast(1))
                        end = true
                    }
                }
            }
        }
    }

    fun searchFilmsByDirector() {
        var end = false
        while (!end) {
            println("Enter the director of the film to see films with that director, or type exit to quit")
            val director = scanner.nextLine()
            if (director.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (director.lowercase() == "exit") {
                    end = true
                } else {
                    val films = filmItb.findFilmByDirector(director)
                    if (films.isEmpty()) {
                        println("No yet films with that director")
                        end = true
                    } else {
                        var toOutput = ""
                        films.forEach{toOutput += "$it\n"}
                        println(toOutput.dropLast(1))
                        end = true
                    }
                }
            }
        }
    }

    fun searchFilmsByMainActor() {
        var end = false
        while (!end) {
            println("Enter the main actor of the film to see films with that main actor, or type exit to quit")
            val mainActor = scanner.nextLine()
            if (mainActor.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (mainActor.lowercase() == "exit") {
                    end = true
                } else {
                    val films = filmItb.findFilmByMainActor(mainActor)
                    if (films.isEmpty()) {
                        println("No yet films with that main actor")
                        end = true
                    } else {
                        var toOutput = ""
                        films.forEach{toOutput += "$it\n"}
                        println(toOutput.dropLast(1))
                        end = true
                    }
                }
            }
        }
    }

    fun searchFilmsByGenre() {
        var end = false
        while (!end) {
            println("Enter the genre of the film to see films with that genre, or type exit to quit")
            val genre = scanner.nextLine()
            if (genre.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (genre.lowercase() == "exit") {
                    end = true
                } else {
                    val films = filmItb.findFilmByGenre(genre)
                    if (films.isEmpty()) {
                        println("No yet films with that genre")
                        end = true
                    } else {
                        var toOutput = ""
                        films.forEach{toOutput += "$it\n"}
                        println(toOutput.dropLast(1))
                        end = true
                    }
                }
            }
        }
    }

    fun searchFilmsByLength() {
        var end = false
        while (!end) {
            println("Enter the length of the film to see films with that length, or type exit to quit")
            val choose = scanner.nextLine()
            if (choose.isBlank()) {
                println("You can't leave the field empty")
            } else {
                if (choose.lowercase() == "exit") {
                    end = true
                } else {
                    val films = filmItb.findFilmByLength(choose.toInt())
                    if (films.isEmpty()) {
                        println("No yet films with that length")
                        end = true
                    } else {
                        var toOutput = ""
                        films.forEach{toOutput += "$it\n"}
                        println(toOutput.dropLast(1))
                        end = true
                    }
                }
            }
        }
    }

    fun searchFilmsNotWatched() {
        println(filmItb.filmsNotWatchedByThisUser(appState) ?: "There is no film yet")
    }

    fun searchFilmsRecomended() {
//        TODO("Not yet implemented ampliacio")
        println("Not yet implemented ampliacio")
    }

    private val search = "Search methods:\n" +
            "1: By title\n" +
            "2: By director\n" +
            "3: By main actor\n" +
            "4: By genere\n" +
            "5: By length\n" +
            "6: Not watched\n" +
            "7: Recomended\n" +
            "0: Return to main menu"

    private fun searchMenu(): Int {
        println(search)
        return scanner.nextLine().toInt()
    }

    fun showMenu(actionExt: Int): Int {
        var action = actionExt
        while (action != 0){
            action = searchMenu()
            when (action){
                1 -> searchFilmsByTitle()
                2 -> searchFilmsByDirector()
                3 -> searchFilmsByMainActor()
                4 -> searchFilmsByGenre()
                5 -> searchFilmsByLength()
                6 -> searchFilmsNotWatched()
                7 -> searchFilmsRecomended()
                0 -> {
                    action = -1
                    break
                }
            }
            action = -1
        }
        return action
    }
}