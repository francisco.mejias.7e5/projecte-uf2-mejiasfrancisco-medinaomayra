package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.ResultFilmItbUsers
import java.util.*
import kotlin.system.exitProcess

data class UserUI(val scanner: Scanner, val filmItb: FilmItb, val appState: AppState) {
    fun addUser(){
        println("Add a username or type exit to quit")
        var end = false
        while (!end){
            val name = scanner.nextLine()
            if (name.isBlank()){
                println("You can't leave the field empty")
            } else {
                if (name.lowercase() == "exit") {
                    end = true
                } else {
                    when (filmItb.addUser(name)) {
                        ResultFilmItbUsers.CORRECT -> end = true
                        ResultFilmItbUsers.ERROR_EXISTING_USER ->
                            println("You can't add a user that exist, try again or type exit to quit")
                        else -> {
                            println("Error on addUser")
                            exitProcess(77)
                        }
                    }
                }
            }
        }
    }

    fun firstAddUserOrCheckIfExist() {
        if (filmItb.users.isNotEmpty()) {
            println("Select the number of user you want start")
            filmItb.users.forEachIndexed { i, it ->
                println("${i+1} -> $it")
            }
            filmItb.changeUser(filmItb.users[scanner.nextLine().toInt()-1].name, appState)
        } else {
            println("Create your first user")
            var end = false
            while (!end) {
                val name = scanner.nextLine()
                if (name.isBlank()) {
                    println("You can't leave the field empty")
                } else {
                    if (name.lowercase() == "exit") {
                        println("That name not allowed")
                    } else {
                        when (filmItb.addUser(name)) {
                            ResultFilmItbUsers.CORRECT -> {
                                end = true
                                filmItb.changeUser(name, appState)
                            }
                            else -> {
                                println("Error on firstAddUser")
                                exitProcess(77)
                            }
                        }
                    }
                }
            }
        }
    }

    fun showCurrentUser() {
        println(appState.currentUser?.name ?: "You're not logged")
    }

    fun showUsers() {
        println(filmItb.showUser)
    }

    fun updateUser() {
        println("Enter your current username")
        var end = false
        while (!end){
            val userNameToChange = scanner.nextLine()
            if (userNameToChange.isBlank()){
                println("You can't leave the field empty")
            } else {
                if (userNameToChange == "exit") {
                    end = true
                } else {
                    if (filmItb.findUserByName(userNameToChange) != null) {
                        println("Enter your new username")
                        val newUserName = scanner.nextLine()
                        if (filmItb.findUserByName(newUserName) == null) {
                            filmItb.updateUser(userNameToChange, newUserName)
                            end = true
                        } else {
                            println("You can't use a name that exist, try again or type exit to quit")
                        }
                    } else {
                        println("This user not exist, try again or type exit to quit")
                    }
                }
            }
        }
    }

    fun removeUser() {
        println("Enter username you want to delete or type exit to quit")
        var end = false
        while (!end) {
            val name = scanner.nextLine()
            if (name.isBlank()){
                println("You can't leave the field empty")
            } else {
                if (name.lowercase() == "exit") {
                    end = true
                } else {
                    when (filmItb.removeUser(name, appState)) {
                        ResultFilmItbUsers.CORRECT -> end = true
                        ResultFilmItbUsers.ERROR_CURRENT_USER ->
                            println("You can't remove a user you are using, try again or type exit to quit")
                        ResultFilmItbUsers.ERROR_NOT_EXISTING ->
                            println("You can't remove a user that not exist, try again or type exit to quit")
                        else -> {
                            println("Error on removeUser")
                            exitProcess(77)
                        }
                    }
                }
            }
        }
    }

    fun changeUser() {
        println("Select username or type exit to quit")
        var end = false
        while (!end){
            val name = scanner.nextLine()
            if (name.isBlank()){
                println("You can't leave the field empty")
            } else {
                if (name == "exit") {
                    end = true
                } else {
                    when (filmItb.changeUser(name, appState)) {
                        ResultFilmItbUsers.CORRECT -> end = true
                        ResultFilmItbUsers.ERROR_CURRENT_USER ->
                            println("You can't change into a user you are using, try again or type exit to quit")
                        ResultFilmItbUsers.ERROR_NOT_EXISTING ->
                        println("You can't change into a user that not exist, try again or type exit to quit")
                        else -> {
                            println("Error on changeUser")
                            exitProcess(77)
                        }
                    }
                }
            }
        }
    }

    fun showStadistics() {
//        TODO("Not yet implemented ampliacio")
        println("Not yet implemented ampliacio")
    }

    private fun usersMenu(): Int {
        println(users)
        return insistOnPickAction()
    }

    private fun insistOnPickAction(): Int {
        try {
            return scanner.nextLine().toInt()
        } catch (e: Exception) {
            println("You must write a number")
            usersMenu()
        }
        return 0
    }

    private val users = "Users:\n" +
            "1: Add user\n" +
            "2: Show my user\n" +
            "3: View users\n" +
            "4: Update user\n" +
            "5: Delete user\n" +
            "6: Change User\n" +
            "7: Show statistics\n" +
            "0: Return to main menu"

    fun showMenu(actionExt: Int): Int {
        var action = actionExt
        while (action != 0) {
            action = usersMenu()
            when (action) {
                1 -> addUser()
                2 -> showCurrentUser()
                3 -> showUsers()
                4 -> updateUser()
                5 -> removeUser()
                6 -> changeUser()
                7 -> showStadistics()
                0 -> {
                    action = -1
                    break
                }
            }
        }
        return action
    }
}