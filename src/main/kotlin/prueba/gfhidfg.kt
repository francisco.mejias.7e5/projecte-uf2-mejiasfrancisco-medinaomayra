package prueba

import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.FilmItb
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model.User

class Persona(val nombre: String, var edad: Int)

data class Persona1(var nombre: String, var edad: Int){
    fun sumEdad(entero: Int){
        edad += entero
    }
}

fun main() {
    val fran = Persona("Antonio", 47)

    val antonio = Persona1("fran", 18)


    fran.edad += 1
    println(fran.edad)


    antonio.sumEdad(1)
    println(antonio.edad)
}