package prueba
import java.util.*
import kotlin.math.sign

fun main() {
    val scanner = Scanner(System.`in`)

    val map = mutableMapOf<String, String>()
    map += "Fran" to "Cubo"
    map += "Alfo" to "Piramide"
    map += "Antonio" to "Escaloide Oblato"
    map += "Jose" to "Semicirculo"
    map += "Miguel" to "Romboide"
    map += "Roman" to "Piraculo"
    map += "Roman" to "Piraculo"
    map += "Roman" to "Piraculo"
    map += "Roman" to "Piraculo"
    map += "Roman" to "Piraculos"
    map += "Romano" to "Piraculos"
    map += "Dani" to "Meneaculo"

    val list = mutableMapOf("User1" to mutableMapOf("FERIA" to 2,
                                                   "Anton" to 3,
                                                   "maria" to 0),
                            "User2" to mutableMapOf("FERIA" to 1))

//    println(list["User1"]!!.values)
//
//    println(list["User1"]?.get("Anton"))
//
//    println(list)
//
//    println(list.values)
//
//    println(list.values.toList())

    println(map["Dani"])
//
//    list.forEach(::println)
}