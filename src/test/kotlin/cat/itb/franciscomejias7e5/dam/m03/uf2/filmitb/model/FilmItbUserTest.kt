package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model

import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui.AppState
import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui.UserUI
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FilmItbUserTest {
    val filmItb = FilmItb()
    val userLao = User("lao")
    val userMarti = User("marti")
    val appState = AppState(User("lao"))

    @Test
    fun addUser() {
        filmItb.addUser("lao")

        assertEquals(mutableListOf(userLao), filmItb.users)
    }

    @Test
    fun showUser() {
        filmItb.addUser("lao")

        assertEquals(listOf(userLao), filmItb.showUser)
    }

    @Test
    fun updateUser() {
        filmItb.addUser("lao")
        filmItb.updateUser("lao", "marti")

        assertEquals(mutableListOf(User("marti")), filmItb.users)
    }

    @Test
    fun removeUser() {
        filmItb.addUser("lao")
        filmItb.addUser("marti")
        filmItb.changeUser("marti", appState)
        filmItb.removeUser("lao", appState)

        assertEquals(mutableListOf(userMarti), filmItb.users)
    }

    @Test
    fun changeUser() {
        filmItb.addUser("lao")
        filmItb.addUser("marti")
        filmItb.changeUser("marti", appState)

        assertEquals(userMarti, appState.currentUser)
    }

    @Test
    fun findUserByName() {
        filmItb.addUser("lao")

        val user = filmItb.findUserByName("lao")

        assertEquals(userLao, user)
    }
}