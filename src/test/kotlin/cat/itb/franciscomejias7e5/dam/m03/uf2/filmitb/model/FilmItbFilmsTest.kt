package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model

import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui.AppState
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class FilmItbFilmsTest {
    val filmItb = FilmItb()
    val appState = AppState(User("lao"))

    val filmElRobotAsesino = Film("El robot asesino",
        "Director",
        "Mateu Yabar",
        "Accion",
        120)

    val filmAliensAlItb = Film("Aliens al ITB",
        "Director",
        "Mateu Yabar",
        "Accion",
        120)

    @Test
    fun addUFilm() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        assertEquals(filmElRobotAsesino, filmItb.films[0])
    }

    @Test
    fun showFilms() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val result = filmItb.showFilms()

        assertEquals(filmElRobotAsesino.toString(), result)
    }

    @Test
    fun userFriendlyFilmsResult() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val result = filmItb.userFriendlyFilmsResult()

        assertEquals(filmElRobotAsesino.toString(), result)
    }

    @Test
    fun removeFilm() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        filmItb.removeFilm(filmElRobotAsesino)

        assertEquals(mutableListOf(), filmItb.films)
    }

    @Test
    fun addToWatchedFlms() {
        filmItb.addToWatchedFilms(filmElRobotAsesino, appState)

        assertEquals(mutableMapOf(appState.currentUser!! to mutableMapOf(filmElRobotAsesino to 1)), filmItb.watchedFilms)
    }

    @Test
    fun showWatchedFilms() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        filmItb.addToWatchedFilms(filmElRobotAsesino, appState)

        val result = filmItb.showWatchedFilms(appState)

        assertEquals(filmElRobotAsesino.toString(), result)
    }

    @Test
    fun addFilmToFavorites() {
        filmItb.addFilmToFavorites(filmElRobotAsesino, appState)

        assertEquals(mutableMapOf(appState.currentUser!! to mutableMapOf(filmElRobotAsesino to 1)), filmItb.favoritesFilms)
    }

    @Test
    fun showFavoritesFilm() {
        filmItb.addFilmToFavorites(filmElRobotAsesino, appState)

        val result = filmItb.showFavoritesFilm(appState)

        assertEquals(filmItb.favoritesFilms[appState.currentUser]!!.keys.first().toString(), result)
    }

    @Test
    fun likesOfThisFilm() {
        filmItb.addFilmToFavorites(filmElRobotAsesino, appState)

        val result = filmItb.likesOfThisFilm(filmElRobotAsesino)

        assertEquals("$filmElRobotAsesino have 1 likes", result)
    }

    @Test
    fun likesOfAllFilms() {
        filmItb.addFilmToFavorites(filmElRobotAsesino, appState)
        filmItb.addFilmToFavorites(filmAliensAlItb, appState)

        val result = filmItb.likesOfAllFilms()

        val list = listOf("$filmElRobotAsesino have 1 likes",
            "$filmAliensAlItb have 1 likes")

        var string = ""

        list.forEach {
            string += "$it\n"
        }
        string = string.dropLast(1)

        assertEquals(string, result)
    }

    @Test
    fun findIfExistSameFilm() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val result = filmItb.findIfExistSameFilm(filmElRobotAsesino)

        assertEquals(filmElRobotAsesino, result!!)
    }


}