package cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.model

import cat.itb.franciscomejias7e5.dam.m03.uf2.filmitb.ui.AppState
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FilmItbSearchTest {
    val filmItb = FilmItb()
    val appState = AppState(User("lao"))

    val filmElRobotAsesino = Film("El robot asesino",
        "Director",
        "Mateu Yabar",
        "Accion",
        120)

    val filmAliensAlItb = Film("Aliens al ITB",
        "Director",
        "Mateu Yabar",
        "Accion",
        120)

    @Test
    fun findFilmByName() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val films = filmItb.findFilmByName("El robot asesino")

        assertEquals(mutableListOf(filmElRobotAsesino), films)
    }

    @Test
    fun findFilmByDirector() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val films = filmItb.findFilmByDirector("Director")

        assertEquals(mutableListOf(filmElRobotAsesino), films)
    }

    @Test
    fun findFilmByMainActor() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val films = filmItb.findFilmByMainActor("Mateu Yabar")

        assertEquals(mutableListOf(filmElRobotAsesino), films)
    }

    @Test
    fun findFilmByGenre() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val films = filmItb.findFilmByGenre("Accion")

        assertEquals(mutableListOf(filmElRobotAsesino), films)
    }

    @Test
    fun findFilmByLength() {
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        val films = filmItb.findFilmByLength(120)

        assertEquals(mutableListOf(filmElRobotAsesino), films)
    }

    @Test
    fun filmsNotWatchedByThisUser() {
        filmItb.addUser("lao")
        filmItb.changeUser("lao", appState)
        filmItb.addFilm("El robot asesino",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        filmItb.addFilm("Aliens al ITB",
            "Director",
            "Mateu Yabar",
            "Accion",
            120)

        filmItb.addToWatchedFilms(filmElRobotAsesino, appState)

        val filmsNotWatched = filmItb.filmsNotWatchedByThisUser(appState)

        assertEquals("$filmAliensAlItb", filmsNotWatched)
    }
}